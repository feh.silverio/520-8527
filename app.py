# PRINT é um comando utilizado pra exibir mensagens na tela.

print("Olá, bem vindo ao curso de python!")

# INPUT é um comando utilizado pra coletar informações do usuario.

input("Qual é o seu nome? ")

# VARIAVEIS são palavras que armazenam um valor.

nome = "Tiago P. Lima"
print(nome)

idade = input("Qual a sua idade? ")
print("Sua idade é:", idade)
