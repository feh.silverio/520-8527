# 1) Em muitos programas, nos é solicitado o preenchimento de algumas informações como
# nome, CPF, idade e unidade federativa. Escreva um script em Python que solicite as
# informações cadastrais mencionadas e que em seguida as apresente da seguinte forma:

# -----------------------------
# Confirmação de cadastro:
# Nome: Guido Van Rossum
# CPF: 999.888.777/66
# Idade: 65
# -----------------------------

print("Bem-vindo ao programa de confirmação de Cadastro")

# VARIAVEIS #
nome = input("Qual seu nome?")
CPF = input("Qual seu CPF?")
idade = input("Qual sua idade?")

# RESULTADOS
print("-----------------------------""\n"
      "Confirmação de cadastro:""\n",
      "Nome:", nome,"\n"
      " CPF:", CPF,"\n"
      " Idade:", idade,"\n"
      "-----------------------------")


      

